For reporting issues, visit [Framagit](https://framagit.org/tom79/fedilab/issues)

[![pipeline status](https://framagit.org/tom79/fedilab/badges/develop/pipeline.svg)](https://framagit.org/tom79/fedilab/commits/develop)
&nbsp;&nbsp;&nbsp;[![Crowdin](https://d322cqt584bo4o.cloudfront.net/mastalab/localized.svg)](https://crowdin.com/project/mastalab)
&nbsp;&nbsp;&nbsp;[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
# Fedilab is a multi-accounts client for Mastodon, Pleroma, Peertube, GNU Social, Friendica and Pixelfed

## Donate

[<img alt="Donate using Liberapay" src="https://img.shields.io/liberapay/patrons/tom79.svg?logo=liberapay"/>](https://liberapay.com/tom79/donate)

## Download

[<img alt='Get it on Google Play' src='./images/get-it-on-play.png' height="80"/>](https://play.google.com/store/apps/details?id=app.fedilab.android)
&nbsp;&nbsp;[<img alt='Get it on F-Droid' src='./images/get-it-on-fdroid.png' height="80"/>](https://f-droid.org/app/fr.gouv.etalab.mastodon)


## Resources

[WIKI](https://fedilab.app/wiki/home/)

[Release notes](https://framagit.org/tom79/fedilab/tags)



<img src="./images/device-2019-02-02-114505.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/device-2019-02-02-114648.png" width="250">

<img src="./images/device-2019-02-02-114713.png" width="560">

<img src="./images/device-2019-02-02-114742.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/device-2019-02-02-114751.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/device-2019-02-02-114808.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/device-2019-02-02-114823.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/device-2019-02-02-114834.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/device-2019-02-02-114845.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/device-2019-02-02-114854.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/device-2019-02-02-114910.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


Lead developer: [toot.fedilab.app/@fedilab](https://toot.fedilab.app/@fedilab)


