package app.fedilab.android.services;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */


import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import app.fedilab.android.R;
import app.fedilab.android.helper.Helper;
import app.fedilab.android.sqlite.DomainBlockDAO;
import app.fedilab.android.sqlite.Sqlite;
import es.dmoral.toasty.Toasty;


/**
 * Created by Thomas on 10/08/20
 * Manage service for downloading tracking db
 */

public class DownloadTrackingDBScriptsService extends Service {


    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    HttpsURLConnection connection = (HttpsURLConnection) new URL("https://sebsauvage.net/hosts/hosts").openConnection();
                    if (connection.getResponseCode() != 200){
                        return;
                    }
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    List<String> domains = new ArrayList<>();
                    while ((line = br.readLine()) != null) {
                        if (line.startsWith("0.0.0.0 ")) {
                            domains.add(line.replace("0.0.0.0 ", "").trim());
                        }
                    }
                    br.close();
                    connection.disconnect();

                    SQLiteDatabase db = Sqlite.getInstance(getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
                    new DomainBlockDAO(DownloadTrackingDBScriptsService.this, db).set(domains);
                    SharedPreferences sharedpreferences = getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(Helper.TRACKING_LAST_UPDATE, Helper.dateToString(new Date()));
                    editor.apply();
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(() -> Toasty.success(DownloadTrackingDBScriptsService.this, getString(R.string.tracking_db_updated), Toast.LENGTH_LONG).show());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
        thread.start();
        return START_NOT_STICKY;
    }




    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



}