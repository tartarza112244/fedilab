package app.fedilab.android.drawers;

import android.content.Intent;
import android.os.Bundle;

import java.util.List;

import app.fedilab.android.activities.OwnerNotificationChartsActivity;
import app.fedilab.android.client.Entities.Notification;
import app.fedilab.android.client.Entities.Status;

/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
public class NotificationsListAdapter extends BaseNotificationsListAdapter {

    public NotificationsListAdapter(boolean isOnWifi, int behaviorWithAttachments, List<Notification> notifications) {
        super(isOnWifi, behaviorWithAttachments, notifications);
    }

    public void notificationStatusChart(Status status) {
        Intent intent = new Intent(context, OwnerNotificationChartsActivity.class);
        Bundle b = new Bundle();
        b.putString("status_id", status.getReblog() != null ? status.getReblog().getId() : status.getId());
        intent.putExtras(b);
        context.startActivity(intent);
    }

}
