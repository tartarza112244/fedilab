Added:
- Gif support for some keyboards.
- Support animated emoji for reactions

Fixed:
- Crash when playing Youtube videos in timelines
- Announcements not displayed
- A crash due to recent changes